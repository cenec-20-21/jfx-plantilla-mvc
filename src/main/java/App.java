import controller.PersonController;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Person;

public class App extends Application {

    @Override
    public void start(Stage stage) {
        try{
            var fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/PersonView.fxml"));
            
            var personController = new PersonController(new Person());
            fxmlLoader.setController(personController);
            
            var root = (Pane)fxmlLoader.load();
            var scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
        catch(IOException exception){
            System.out.println(exception.getMessage());
        }
    }

    public static void main(String[] args) {
        launch();
    }
}